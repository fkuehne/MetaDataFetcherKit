# MetaDataFetcherKit

[![Version](https://img.shields.io/cocoapods/v/MetaDataFetcherKit.svg?style=flat)](http://cocoapods.org/pods/MetaDataFetcherKit)
[![License](https://img.shields.io/cocoapods/l/MetaDataFetcherKit.svg?style=flat)](http://cocoapods.org/pods/MetaDataFetcherKit)
[![Platform](https://img.shields.io/cocoapods/p/MetaDataFetcherKit.svg?style=flat)](http://cocoapods.org/pods/MetaDataFetcherKit)

## Requirements

Depends on AFNetworking, requires iOS 7.0, OS X 10.9 or tvOS 9.0.

## Installation

MetaDataFetcherKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MetaDataFetcherKit"
```

## Author

Felix Paul Kühne, fkuehne@videolan.org

## License

MetaDataFetcherKit is available under the LGPLv2.1 license. See the LICENSE file for more info.
