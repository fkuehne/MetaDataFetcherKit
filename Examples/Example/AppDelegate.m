//
//  AppDelegate.m
//  Example
//
//  Created by Felix Paul Kühne on 01.05.20.
//  Copyright © 2020 VideoLAN. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()
{
    ViewController *_viewController;
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _viewController = [[ViewController alloc] initWithNibName:nil bundle:nil];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = _viewController;
    [self.window makeKeyAndVisible];

    return YES;
}

@end
