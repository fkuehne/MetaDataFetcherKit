//
//  AppDelegate.h
//  Example
//
//  Created by Felix Paul Kühne on 01.05.20.
//  Copyright © 2020 VideoLAN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

